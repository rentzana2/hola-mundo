#FROM php:7.4.1-apache-buster
FROM php:8.0.6-apache-buster

COPY src/ /var/www/html

EXPOSE 80
